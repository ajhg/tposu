﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mirrorObjectRoation : MonoBehaviour 
{

	public GameObject roationObject; 

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		this.transform.eulerAngles = new Vector3(
			this.transform.eulerAngles.x,
			roationObject.transform.eulerAngles.y,
			this.transform.eulerAngles.z
		);
		//Debug.Log(this.transform.eulerAngles);
	}
}
