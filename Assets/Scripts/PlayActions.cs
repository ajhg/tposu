﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Drawing;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using osuElements.Beatmaps;
//using TPoSu;

[RequireComponent(typeof(AudioSource))]
public class PlayActions : MonoBehaviour {

	public InputField BeatmapDir;
	public AudioSource ButtonClick;
	public Button PlayButton;

	public String BeatMapDir;

	// Use this for initialization
	void Start () 
	{
		PlayButton.onClick.AddListener(PlayButtonClick);
		SceneManager.LoadSceneAsync("LoadingScreen");
	}
	
	// Update is called once per frame
	void PlayButtonClick() 
	{
		ButtonClick.Play();
		BeatMapDir = GameObject.Find("BeatMapDirectoryText").GetComponent<Text>().text;
		SceneManager.SetActiveScene(SceneManager.GetSceneByName("GameField"));
	}
}
