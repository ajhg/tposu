﻿using System.Collections;
using System.Collections.Generic;           
using UnityEngine;

public class QuickTurn : MonoBehaviour
{

    public int rotationSpeed = 10;

    public bool rotated = true;
    public int currentAngle = 0;
    public int direction = 1;
    public bool logDebug = false;

    // Start is called before the first frame update
    void Start() {
        if (rotationSpeed > 180)
        {
            rotationSpeed = 180;
        }
        rotated = true;
        currentAngle = 0;
        randomiseDirection();
    }

    // Update is called once per frame
    void Update()
    {
        var debugAngle = 0;
        if (Input.GetButton("QuickTurn") && rotated) 
        {
            rotated = false;
            randomiseDirection();
            //Update();
        }
        else if (!rotated) 
        {
            var rotationAngle = 0;
            if ((currentAngle + rotationSpeed) > 180)
            {
                rotationAngle = (currentAngle + rotationSpeed) - 180;
            } 
            else 
            {
                rotationAngle = rotationSpeed;
            }

            this.transform.eulerAngles = new Vector3(
                this.transform.eulerAngles.x,
                this.transform.eulerAngles.y + (rotationAngle * direction),
                this.transform.eulerAngles.z
            );

            if (rotationAngle > 0) 
            {
                currentAngle += (rotationAngle * -1);
            }
            else 
            {
                currentAngle += rotationAngle;
            }

            if (currentAngle >= 180)
            {
                rotated = true;
                //Start();
                if (rotationSpeed > 180) 
                {
                    rotationSpeed = 180;
                }
                rotated = true;
                currentAngle = 0;
                randomiseDirection();
            }

            debugAngle = rotationAngle;
        }
        if (logDebug) {
			Debug.Log("rotated: " + rotated);
            Debug.Log("currentAngle: " + currentAngle);
            Debug.Log("direction: " + direction);
            Debug.Log("rotationAngle: " + debugAngle);
        }
    }

    void randomiseDirection() {
        if (Random.Range(1, 3) == 1) 
        {
            direction = 1;
        }
        else
        {
            direction = -1;
        }
    }

}
