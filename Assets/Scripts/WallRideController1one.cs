﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using UnityStandardAssets.CrossPlatformInput;
public class WallRideController1one : MonoBehaviour 
{

// https://www.footnotesforthefuture.com/words/wall-running-1/
    
    private RigidbodyFirstPersonController player;
    private bool onWall = false;
    private Vector3 ridingDirection;

    public float rayDistance;
    public bool logDebug = false;

    public bool isOnWall() 
    {
        return onWall;
    }

	void Start () {
        player = GameObject.FindWithTag("Player").GetComponent<RigidbodyFirstPersonController>();
        player.enabled = true;
    }
	
	void Update () 
    {

        if (CrossPlatformInputManager.GetButtonDown("Jump") && onWall) 
        {
            player.enabled = true;
            onWall = false;
        }

        if (!player.Grounded && !onWall) {
            float leftDistance = 0;
            float rightDistance = 0;

            Vector3 leftWallRayDirection = new Vector3(
                this.transform.eulerAngles.x,
                this.transform.eulerAngles.y - 90,
                this.transform.eulerAngles.z
            );

            Vector3 rightWallRayDirection = new Vector3(
                this.transform.eulerAngles.x,
                this.transform.eulerAngles.y + 90,
                this.transform.eulerAngles.z
            );

            RaycastHit leftHit; 
            RaycastHit rightHit;

            if (Physics.Raycast(this.transform.position, leftWallRayDirection, out leftHit, rayDistance, 9) && (leftHit.collider.tag == "isWallRideable")) 
            {
                if (logDebug)
                {
                    Debug.Log("left wall is ridable");
                }
                leftDistance = leftHit.distance;
            }

            if (Physics.Raycast(this.transform.position, rightWallRayDirection, out rightHit, rayDistance, 9) && (rightHit.collider.tag == "isWallRideable"))
            {
                if (logDebug)
                {
                    Debug.Log("right ray hit");
                }
                rightDistance = rightHit.distance;
            }

            if ((leftDistance + rightDistance) <= 0) 
            {
                Debug.Log("Fatil collition error with object \"" + this.name + "\" at the cordinates " + this.transform.position + " at the time " + Time.timeScale + ".");
                throw new Exception("Fatil collition error with object \"" + this.name + "\" at the cordinates " + this.transform.position + " at the time " + Time.timeScale + ".");
            }

            if ((leftDistance < rightDistance)) 
            {
                if (logDebug)
                {
                    Debug.Log("discarding right ray and using left ray");
                    Debug.Log(leftHit.normal);
                    Debug.Log(Vector3.up);    
                }                      
                ridingDirection = Vector3.Cross(leftHit.normal, Vector3.up);
            }
            else
            {
                if (logDebug)
                {
                    Debug.Log("discarding left ray and using right ray");
                    Debug.Log(rightHit.normal);
                    Debug.Log(Vector3.up);
                }
                ridingDirection = Vector3.Cross(rightHit.normal, Vector3.up);
            }

            if (logDebug)
            {
                Debug.Log(this.transform.position);
            }
        }
        else
        {   
            onWall = true;
            player.enabled = false;
            wallRide();
        }
        
	}

    void wallRide() {
        this.transform.position = ridingDirection;
    }
}
