﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Drawing;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
//using UnityStandardAssets.Characters.FirstPerson;
//using UnityStandardAssets.CrossPlatformInput;

public class MovementTweaks : MonoBehaviour 
{

	private GameObject player;

	public int downValue = 10;
	public float quickTurnSpeed = 10;
	//public GameObject cameraRotationObject;
	public int wallJumpForce = 10;
	
	public bool logDebug = false;

	// Use this for initialization
	void Start () 
	{
		player = GameObject.FindWithTag("Player");
		if (quickTurnSpeed > 180)
        {
            quickTurnSpeed = 180;
        }
	}
	
	// Update is called once per frame
	void Update () 
	{
		//Debug.Log(player.GetComponent<RigidbodyFirstPersonController>().getGroundContactNormal());
		if (Input.GetButtonDown("Ctrl") && !player.GetComponent<RigidbodyFirstPersonController>().Grounded) 
		{	
			player.GetComponent<RigidbodyFirstPersonController>().doGroundCheck();
			this.transform.position += (Vector3.down * downValue) * Time.deltaTime;
		}

		if (Input.GetButtonDown("QuickTurn")) 
		{
			MakeQuickTurn();
		}
			
		if (player.GetComponent<wallRideControllerTwo>().enabled) 
		{
			if (player.GetComponent<wallRideControllerTwo>().isWallRiding() && Input.GetButtonDown("Jump")) 
			{
				this.transform.position += (Vector3.up * wallJumpForce) * Time.deltaTime;
			}
		}
		

	}

	public void MakeQuickTurn() //float quickTurnAmount
	{
		
		int direction = 1;

		if (UnityEngine.Random.Range(1, 3) == 1) 
		{
			direction = 1;
		}
		else
		{
			direction = -1;
		}

		float quickTurnAmount = quickTurnSpeed * direction;
		if (logDebug) Debug.Log(quickTurnAmount);
		// cameraRotationObject.transform.eulerAngles = new Vector3(
		// 	cameraRotationObject.transform.eulerAngles.x,
		// 	cameraRotationObject.transform.eulerAngles.y + quickTurnAmount,
		// 	cameraRotationObject.transform.eulerAngles.z
		// );
		//this.transform.eulerAngles = new Vector3(
		// 	this.transform.eulerAngles.x,
		// 	this.transform.eulerAngles.y + quickTurnAmount,
		// 	this.transform.eulerAngles.z
		// );
		Quaternion rotation = Quaternion.Euler(0, quickTurnAmount, 0);
		//this.transform.eulerAngles = rotation;
	}
}
