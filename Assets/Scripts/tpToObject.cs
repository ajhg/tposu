using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tpToObject : MonoBehaviour 
{

	public GameObject objectToTP_to;
	public float xOffset;
	public float yOffset;
	public float zOffset;

	// Use this for initialization
	void Start () 
	{ 
		this.transform.position = new Vector3(
			objectToTP_to.transform.position.x + xOffset,
			objectToTP_to.transform.position.y + yOffset, 
			objectToTP_to.transform.position.z + zOffset
		); 
	}
	// Update is called once per frame
	void Update () 
	{ 
		this.transform.position = new Vector3(
			objectToTP_to.transform.position.x + xOffset,
			objectToTP_to.transform.position.y + yOffset, 
			objectToTP_to.transform.position.z + zOffset
		); 
	}
}