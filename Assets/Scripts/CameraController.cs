﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    #region Singleton

    public static CameraController instance;

    void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("More than one instance of this" + this.name + "found!");
            return;
        }
        instance = this;
    }

    #endregion

    [SerializeField] GameObject player;
    Transform target;                               // The target object to follow
    Camera camera;
    Transform pivot;

    [SerializeField] float moveSpeed = 10f;                     // How fast the rig will move to keep up with the target's position.
    [Range(0f, 10f)] [SerializeField] float mouseSensitivityX = 1f;     // How fast the rig will rotate from user input.
    [Range(0f, 10f)] [SerializeField] float mouseSensitivityY = 1f;     // How fast the rig will rotate from user input.
    [SerializeField] float tiltMax = 75f;                       // The maximum value of the y axis rotation of the pivot.
    [SerializeField] float tiltMin = 60f;                       // The minimum value of the y axis rotation of the pivot.
    [SerializeField] float pivotSpeed = 3.0f;

    [HideInInspector] public float cameraX;                              // The pivot's x axis rotation.
    [HideInInspector] public float cameraY;                              // The rig's y axis rotation.
    const float lookDistance = 100f;            // How far in front of the pivot the character's look target is.
    Vector3 pivotEulers;
    Quaternion pivotTargetRot;
    Quaternion transformTargetRot;

    void Start()
    {
        // Find the camera in the object hierarchy
        target = player.transform;
        camera = GetComponentInChildren<Camera>();
        pivot = camera.transform.parent;

        // Lock and hide the cursor
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        pivotEulers = pivot.rotation.eulerAngles;
    }

    void Update()
    {
        HandleRotationMovement();
    }

    void FixedUpdate()
    {
        FollowTarget(Time.deltaTime);
    }

    void HandleRotationMovement()
    {
        if (Time.timeScale < float.Epsilon)
        {
            return;
        }
        
        var x = Input.GetAxis("Mouse X");
        var y = Input.GetAxis("Mouse Y");

        cameraX += x * mouseSensitivityX;
        cameraY -= y * mouseSensitivityY;
        cameraY = Mathf.Clamp(cameraY, -tiltMin, tiltMax);

        transformTargetRot = Quaternion.Euler(0f, cameraX, 0);
        pivotTargetRot = Quaternion.Euler(cameraY, pivotEulers.y, pivotEulers.z);

        pivot.localRotation = pivotTargetRot;
        transform.localRotation = transformTargetRot;
    }

    void FollowTarget(float deltaTime)
    {
        if (target == null)
        {
            return;
        }

        transform.position = Vector3.Lerp(transform.position, target.position, deltaTime * moveSpeed);
    }
}
