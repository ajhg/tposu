﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class basicMovementScript : MonoBehaviour 
{

	public Camera mainCam; 
	public float walk = 5.0f;
    public float speed = 6.0f;
    public float gravity = 4;
	public float sprintSpeed = 9.0f;
	public float jumpSpeed = 10;

	private CharacterController characterController;

	// Use this for initialization
	void Start () 
	{
		characterController = GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		
		if (Input.GetButton("Shift")) 
		{
			speed = sprintSpeed;
		} else speed = walk;

		float mainCamY = mainCam.transform.eulerAngles.y;
		float deltaX = (Input.GetAxis("Horizontal") + mainCamY) * speed;
        float deltaZ = (Input.GetAxis("Vertical") + mainCamY) * speed;
		Debug.Log(deltaX);
		Debug.Log(deltaZ);
        Vector3 movement = Vector3.ClampMagnitude(transform.TransformDirection(new Vector3(deltaX, 0, deltaZ)), speed);

		//Vector3 temp = new Vector3(deltaX);
		this.transform.position += movement; 	

	}
}
