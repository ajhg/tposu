﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityStandardAssets.CrossPlatformInput;

public class freeMovementCamera : MonoBehaviour {

	public Vector3 startCoords = new Vector3(0, 0, 0);
	public int walkDevider = 2;
    public int movementSpeed = 6;
	public int sprintMultiplyer = 2;

	private bool doubleSpeed;
	private bool halfSpeed;
    private int movementSpeedConstant;
	// private bool tapping;
	// private float lastTap;
	// private float tapTime;

	void Start () {
		this.transform.position = startCoords;
		movementSpeedConstant = movementSpeed;
	}
	
	void Update () {
		movementSpeed = movementSpeedConstant;
		if (Input.GetButton("Ctrl")) 
		{
			movementSpeed /= walkDevider;
			halfSpeed = true;
		}
		if (Input.GetButton("Alt")) 
		{
			movementSpeed *= sprintMultiplyer;
			doubleSpeed = true;
		}
		if (Input.GetButton("Jump")) 
		{
			this.transform.position += (Vector3.up * movementSpeed) * Time.deltaTime;
		}
		if (Input.GetButton("Shift")) 
		{
			this.transform.position += (Vector3.down * movementSpeed) * Time.deltaTime;
		}
		if (Input.GetAxis("Vertical") > 0)
		{
			this.transform.position += (Vector3.forward * movementSpeed) * Time.deltaTime;
		}
		if (Input.GetAxis("Vertical") < 0)
		{
			this.transform.position += (Vector3.back * movementSpeed) * Time.deltaTime;
		}
		if (Input.GetAxis("Horizontal") > 0)
		{
			this.transform.position += (Vector3.right * movementSpeed) * Time.deltaTime;
		}
		if (Input.GetAxis("Horizontal") < 0)
		{
			this.transform.position += (Vector3.left * movementSpeed) * Time.deltaTime;
		}
		if ((Input.GetAxis("Horizontal") == 0) &&
			(Input.GetAxis("Vertical") == 0) && 
			(!Input.GetButton("Shift")) && 
			(!Input.GetButton("Jump")))	
		{	
			this.transform.position += Vector3.zero * Time.deltaTime;
		}
		if (Input.GetButton("QuickTurn"))
		{
			this.transform.position = startCoords;
		}
	}

	public int getMovementSpeed() 
	{
		if (doubleSpeed) return 2;
		else if (halfSpeed) return 0;
		else return 1;
	}
}
