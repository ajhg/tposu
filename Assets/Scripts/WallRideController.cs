﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class WallRideController : MonoBehaviour 
{

// https://www.footnotesforthefuture.com/words/wall-running-1/

    public GameObject gameObjectMesh;
    public float rayDistance;

	void Start () 
    {
        
    }
	
	void Update () 
    {
        Vector3 leftWallRayDirection = new Vector3(
    		gameObjectMesh.transform.eulerAngles.x,
        	gameObjectMesh.transform.eulerAngles.y - 90,
        	gameObjectMesh.transform.eulerAngles.z
    	);
        Vector3 rightWallRayDirection = new Vector3(
    		gameObjectMesh.transform.eulerAngles.x,
        	gameObjectMesh.transform.eulerAngles.y + 90,
        	gameObjectMesh.transform.eulerAngles.z
    	);

        RaycastHit leftHit; 
        RaycastHit rightHit;

		if (Physics.Raycast(gameObjectMesh.transform.position, leftWallRayDirection, out leftHit, rayDistance, 9)) 
        {
            Debug.Log("left ray hit");
            if (leftHit.collider.tag == "isWallRideable") 
            {
                Debug.Log("left wall is ridable");
                if (Physics.Raycast(gameObjectMesh.transform.position, rightWallRayDirection, out rightHit, rayDistance, 9)) 
                {
                    Debug.Log("right ray hit");
                    if (rightHit.collider.tag == "isWallRideable") 
                    {
                        Debug.Log("right wall is ridable");
                        if (leftHit.distance < rightHit.distance) 
                        {
                            Debug.Log("discarding right ray");
                            Debug.Log("using left ray");
                            Debug.Log(leftHit.normal);
                            Debug.Log(Vector3.up);                            
                            this.transform.position = Vector3.Cross(leftHit.normal, Vector3.up);
                            Debug.Log(this.transform.position);
                        }
                        else
                        {
                            Debug.Log("discarding left ray");
                            Debug.Log("using right ray");
                            Debug.Log(rightHit.normal);
                            Debug.Log(Vector3.up);
                            this.transform.position = Vector3.Cross(rightHit.normal, Vector3.up);
                            Debug.Log(this.transform.position);
                        }
                    }
                }
                else
                {
                    Debug.Log("using left ray");
                    Debug.Log(leftHit.normal);
                    Debug.Log(Vector3.up);  
                    this.transform.position = Vector3.Cross(leftHit.normal, Vector3.up);
                    Debug.Log(this.transform.position);
                }
            }
        } 
        else if (Physics.Raycast(gameObjectMesh.transform.position, rightWallRayDirection, out rightHit, rayDistance, 9))
        {
            Debug.Log("right ray hit");
            if (rightHit.collider.tag == "isWallRideable")
            { 
                Debug.Log("using right ray");
                Debug.Log(rightHit.normal);
                Debug.Log(Vector3.up);
                this.transform.position = Vector3.Cross(rightHit.normal, Vector3.up);
                Debug.Log(this.transform.position);
            }
        }
	}
}
