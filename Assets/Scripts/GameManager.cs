﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Drawing;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using osuElements.Beatmaps;
//using TPoSu;

public class GameManager : MonoBehaviour
{

    //public List<GameObject> hitObjectStack = new List<GameObject>();
    public int vDistance = 5;
    public List<GameObject> stack = new List<GameObject>();

    public String TestDir = "B:\\Documents\\School\\U\\S\\S\\GP\\testMap\\375668 Kazumi Totaka - Mii Plaza\\Kazumi Totaka - Mii Plaza (ayygurl) [Easy].osu";
    public Beatmap Beatmap; 

    public GameObject HitCircleObject;
    public GameObject Slider;

    public GameObject TPoSuStack;

    // Start is called before the first frame update
    void Start()
    {
        Beatmap = new Beatmap(TestDir); //GameObject.Find("BeatMapDirectoryText").GetComponent<Text>().text
        for (int i = 0; i < Beatmap.HitObjects.Count; i++) {
            if (Beatmap.HitObjects[i].Type == HitObjectType.HitCircle)
            {
                stack.Add(Instantiate(
                    HitCircleObject,
                    new Vector3(
                        Beatmap.HitObjects[i].StartPosition.X,
                        i * vDistance,
                        Beatmap.HitObjects[i].StartPosition.Y
                        ),
                    new Quaternion(0, 0, 0, 1),
                    TPoSuStack.transform
                    ));
            }
            else if (Beatmap.HitObjects[i].Type == HitObjectType.Slider)
            {
                // Add slider
            }
            else if (Beatmap.HitObjects[i].Type == HitObjectType.Spinner) 
            {
                stack.Add(Instantiate(
                   HitCircleObject,
                   new Vector3(0, i * vDistance, 0),
                   new Quaternion(0, 0, 0, 1),
                   TPoSuStack.transform
                   ));
            }
            
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (this.transform.position.y )
    }
}
