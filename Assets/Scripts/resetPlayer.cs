﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class resetPlayer : MonoBehaviour {

	public float floorValue = -10;

	public Vector3 resetCoords = new Vector3(0, 2.5f, 0);

	// Use this for initialization
	//void Start () {}
	
	// Update is called once per frame
	void Update () {
		if (this.transform.position.y < floorValue)
		{
			this.transform.position = resetCoords;
		}
	}
}
