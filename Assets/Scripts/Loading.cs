﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loading : MonoBehaviour
{
    private GameObject Player;

    public float MoveSpeed = 10f;

    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.FindWithTag("Player");
        Player.transform.position = new Vector3(
            Player.transform.position.x,
            Player.transform.position.y,
            -2000
        );
    }

    // Update is called once per frame
    void Update()
    {
        if (Player.transform.position.y >= 0)
        {
            Player.transform.position = new Vector3(
                Player.transform.position.x,
                Player.transform.position.y,
                -2000
            );
        }
        Player.transform.position = new Vector3(
            Player.transform.position.x,
            Player.transform.position.y,
            Player.transform.position.z + MoveSpeed
        );
    }
}
