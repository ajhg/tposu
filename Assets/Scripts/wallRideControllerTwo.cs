﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using UnityStandardAssets.CrossPlatformInput;

public class wallRideControllerTwo : MonoBehaviour {

	//private GameObject player;
	private bool wallRiding = false;

	public int wallRideSpeed = 10;

	// Use this for initialization
	void Start () {
		//player = GameObject.FindWithTag("Player");
		wallRiding = false;
	}
	
	// Update is called once per frame
	// void Update () {
		
	// }

	void OnCollisionEnter(Collision collision)
    {

        if (!this.GetComponent<RigidbodyFirstPersonController>().Grounded) 
		{
			// Debug.Log("Points colliding: " + collision.contacts.Length);
        	Debug.Log("Colliution!");
			wallRiding = true;
			this.GetComponent<Rigidbody>().useGravity = false;
			this.transform.position += (Vector3.forward * wallRideSpeed) * Time.deltaTime;
		}
    }

	void OnCollisionStay(Collision collision)
	{
		if (!this.GetComponent<RigidbodyFirstPersonController>().Grounded) 
		{
			this.transform.position += (Vector3.forward * wallRideSpeed) * Time.deltaTime;
			this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY;
        }
	}

	void OnCollisionExit(Collision collision)
	{
		wallRiding = false;
		this.GetComponent<Rigidbody>().useGravity = true;
		this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
		this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
	}

	public bool isWallRiding()
	{
		return wallRiding;
	}
}
