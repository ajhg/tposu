﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class thirdPersonMovementController : MonoBehaviour {

	public float speed = 6.0F;
	public float runSpeed = 9.0F;
	public float gravity = 20.0F;

	private Vector3 moveDirection = Vector3.zero;
	private bool grounded = false;

	// Use this for initialization
	//void Start () {}
	
	// Update is called once per frame
	//void Update () {}

	void FixedUpdate()
	{
		if (grounded)
			{
			// We are grounded, so recalculate movedirection directly from axes
			moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")); //Determine the player's forward speed based upon the input.
		
			moveDirection = transform.TransformDirection(moveDirection); //make the direction relative to the player.
			if (Input.GetButton("Shift"))
			{
				moveDirection *= runSpeed;
			}
			else
			{
				moveDirection *= speed;
			}
		}
	
		// Apply gravity
		moveDirection.y -= gravity * Time.deltaTime;
	
		// Move the controller
		CharacterController controller  = this.GetComponent<CharacterController>();
		CollisionFlags flags = controller.Move(moveDirection * Time.deltaTime);
	 
		grounded = (flags & CollisionFlags.CollidedBelow) != 0;
	}
}
